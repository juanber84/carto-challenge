#### Programming Language

The challenge of code is writen in **nodejs** languaje. The **project** is structured like this.

```
+ .circleci (ci)
+ data (dat files)
+ dist (frontend compilation code)
+ doc (markdown documentation)
+ src (main code)
    + app (server app code)
        + controllers 
            - activityController.js 
            - mainController.js (healtCheck endpoint)
        + services
            - activityService (core service)
    + lib (code utils)
        - db.js
        - time.js
        - dataParser.js
        - utils.js
    + front (frontend app code)
    - bootstrap.js (middlewares)
    - routes.js (router)
    - index.js (main file)
+ reports (testing reports)
+ test (testing code)
```

#### Develop quality tools
- tdd/test (jest, supertest)
- Code style (prettier, lint, validate-commit)
- ci (circleci)
- Version control system (git)

#### Main core features
- I have 2 controllers, one of them to get list and filter activities and the other to get recommendations feature. Both controllers has the same funcionality, they must return a list but with diferent limit.
- Both controller has included his specific filters in function inside an array, if you want include new filters you can push the new filter in this array and his filters will be applied.
- Controllers execute the same service activityService.
- Activity Service get all activities and decided if activities are valid. It execute a simple string comparation or a complex date range validation. After, it will sort the activities by hours spent and return the list with limit filter applied.

#### Extra features
- Do not recommend an outdoors activity on a rainy day. I have included an aemet method in controllers to filter location. I can request the weather in aemet website and change value of location filter to indoors or included it automatically.
- Support getting information about activities in multiple cities. This feature is supported in src/lib/db.j, the segovia.json data are being returned in api rest services.
- Extend the recommendation API to fill the given time range with multiple activities. I think that the recommendations endpoint will would must return lot of results and the limit of 1 result in recommendations must be removed, so the final user will can choose his preferer activity of them and request other activity to api recommendations with other time range.
- Front app developed with vue included.