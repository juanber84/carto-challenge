// vue.config.js
module.exports = {
  pages: {
    index: {
      entry: 'src/front/main.js',
      template: 'src/front/public/index.html',
      filename: 'index.html'
    }
  }
}