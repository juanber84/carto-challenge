'use strict'

const db = require('../../lib/db')
const _ = require('lodash')
const { getMinutesInDay, rangeDiff, minutesToTime} = require('../../lib/time')
const debug = require('debug')('app:activityService')

module.exports = {
  list(filters, sort = null, limit = null) {
    // Get data normalized
    const allActivities = db.getAllActivities()
    // Filter data
    if (filters) {
      const filterFeatures = allActivities.features.filter(d => {
        debug('♨  ♨  ♨  ♨  activity', d.properties.name)
        for (const f in filters) {
          if (f === 'time-range') {
            // Split user time
            const [day, freeTimeStart, freeTimeEnd] = filters[f].split('-')
            if (d.properties.opening_hours[day].length === 0){
              debug('☠  ☠  ☠  ☠  discarded-by', f, 'opening_hours', day, 'not-oppened')
              return false
            } else {
              // Split activity time
              const [openActivity, closeActivity] = d.properties.opening_hours[day][0].split('-')
              debug('✎  ✎  ✎  ✎  analyze', JSON.stringify({
                freeTimeStart, freeTimeEnd, openActivity, closeActivity
              }))
              // Get common range of time 
              const commonRange = rangeDiff([getMinutesInDay(freeTimeStart), getMinutesInDay(freeTimeEnd)], [getMinutesInDay(openActivity), getMinutesInDay(closeActivity)])
              // if common range is null return false
              if (!commonRange) {
                debug('☠  ☠  ☠  ☠  discarded-by', f, 'commonRange', 'empty')
                return false
              }
              debug('✎  ✎  ✎  ✎  analyze', 'commonRange', commonRange, { start: minutesToTime(commonRange.start), end: minutesToTime(commonRange.end) })              
              // Get total of minutes in range
              const totalMinutesInRange = commonRange.end - commonRange.start
              // Get minutes of duration in activity
              const activiTyDurationMinutes = d.properties.hours_spent * 60
              debug('✎  ✎  ✎  ✎  analyze', 'totalMinutesInRange', totalMinutesInRange, 'hours_spent', d.properties.hours_spent, 'activiTyDurationMinutes', activiTyDurationMinutes)  
              // If user dont hace time return false
              if (activiTyDurationMinutes > totalMinutesInRange) {
                debug('☠  ☠  ☠  ☠  discarded-by', f, 'insufficient-time')
                return false
              }
            }
          } else {
            if (!d.properties[f] || d.properties[f] !== filters[f]) {
              debug('☠  ☠  ☠  ☠  discarded-by', f, JSON.stringify({ expected: d.properties[f], result: filters[f]}))
              return false
            }
          }
        }
        debug('✓  ✓  ✓  ✓  selected', d.properties.name)
        return true
      })
      allActivities.features = filterFeatures
    }

    if (sort)
      debug('reorder-activities', 'properties.' + sort[0])
      allActivities.features = _.orderBy(
        allActivities.features,
        ['properties.' + sort[0]],
        [sort[1]]
      )

    if (limit && limit === 1) return allActivities.features[0]
    // TODO multiples results

    return allActivities
  },
}

// TODO: Extend the recommendation API to fill the given time range with multiple activities
