'use strict'

const activityService = require('../services/activityService')
const debug = require('debug')('app:activityController')

module.exports = {
  listDeliver(req) {
    const validFilters = ['category', 'location', 'district', 'city']
    const filters = getFilters(req, validFilters)
    debug('list', JSON.stringify(filters))
    aemet(filters)
    return activityService.list(filters, ['hours_spent', 'desc'])
  },
  recommendationsDeliver(req) {
    const validFilters = ['category', 'city', 'time-range']
    const filters = getFilters(req, validFilters)
    debug('recommendations', JSON.stringify(filters))   
    aemet(filters)
    return activityService.list(filters, ['hours_spent', 'desc'], 1)
  },
}

function aemet(filters) {
    // TODO: Do not recommend an outdoors activity on a rainy day
    // You can request to aemet website and change value of location filter to indoors or included it automatically
    return filters
}

function getFilters(req, validFilters) {
  const filters = {}
  validFilters.forEach(f => {
    if (req.query[f]) filters[f] = req.query[f].toLowerCase()
  })
  return Object.keys(filters).length > 0 ? filters : null
}
