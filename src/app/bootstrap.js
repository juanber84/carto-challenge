'use strict'

const debug = require('debug')('app:bootstrap')
const expressDeliver = require('express-deliver')
const morgan = require('morgan')
const compression = require('compression')
const cors = require('cors')
const bodyParser = require('body-parser')
const helmet = require('helmet')

module.exports = app => {
  expressDeliver(app)

  app.set('x-powered-by', false)
  app.set('etag', false)

  app.use(helmet())
  debug('helmet', true)

  app.use(
    morgan('dev', {
      skip: function(req, res) {
        return res.statusCode < 400
      },
    })
  )

  app.use(compression())
  debug('compression', true)

  app.use(cors())
  debug('cors', true)

  // Parses http body
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())
  debug('bodyParser', true)

  require('./routes')(app)
  debug('routes', true)

  expressDeliver.errorHandler(app)
}
