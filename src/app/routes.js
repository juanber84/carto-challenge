'use strict'

const mainController = require('./controllers/mainController')
const activityController = require('./controllers/activityController')
const express = require('express')
const path = require('path')

module.exports = app => {
  app.use(express.static(path.join(__dirname + './../../dist')))
  app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + './../../dist/index.html'));
  })  
  app.get('/health', mainController.index)
  app.get('/activities', activityController.listDeliver)
  app.get('/recommendations', activityController.recommendationsDeliver)
}
