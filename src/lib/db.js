'use strict'

const dataParser = require('./dataParser')
const dataMadrid = require('../../data/madrid.json')
const dataSegovia = require('../../data/segovia.json')

module.exports = {
  getAllActivities() {
    // TODO: automatic file loading
    // Include city in data
    const dbData = _loadData([
      { city: 'madrid', data: dataMadrid },
      { city: 'segovia', data: dataSegovia },
    ])
    return Object.assign({}, dataParser(dbData))
  },
}

function _loadData(files) {
  const data = []
  files.forEach(f => {
    f.data.forEach(d => {
      d.city = f.city
      data.push(d)
    })
  })
  return data
}
