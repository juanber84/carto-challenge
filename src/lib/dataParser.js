'use strict'

const GeoJSON = require('geojson')

module.exports = data => {
  return GeoJSON.parse(
    data.map(d => {
      // toLower properties
      d.category = d.category.toLowerCase()
      d.location = d.location.toLowerCase()
      d.district = d.district.toLowerCase()
      // Move properties
      d.lat = d.latlng[0]
      d.lng = d.latlng[1]
      return d
    }),
    { Point: ['lat', 'lng'], exclude: ['latlng'] }
  )
}
