'use strict'

const _ = require('lodash')
const { transformNumbers, reTransformNumbers } = require('./utils')

module.exports = {
  getMinutesInDay(time) {
    const [hour, minute] = time.split(':')
    return (parseInt(transformNumbers(hour)) * 60) + parseInt(minute)
  },
  rangeDiff(range1, range2){
    const arrayNumberRange1 = _.range(range1[0], range1[1] + 1)
    const arrayNumberRange2 = _.range(range2[0], range2[1] + 1)
    const intersection = _.sortBy(_.intersection(arrayNumberRange1, arrayNumberRange2))
    if (intersection.length === 0)
      return null
    return {
      start: intersection[0],
      end: intersection[intersection.length - 1]
    }
  },
  minutesToTime(totalMinutes) {
    const transformTime = totalMinutes / 60
    const hours = Math.floor(transformTime)
    const minutes = Math.round((transformTime - Math.floor(transformTime)) * 60)
    return reTransformNumbers(hours) + ':' + reTransformNumbers(minutes)
  }  
}
