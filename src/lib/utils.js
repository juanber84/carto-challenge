'use strict'

module.exports = {
  transformNumbers(number) {
    if (number === '00') return 0
    if (number === '01') return 1
    if (number === '02') return 2
    if (number === '03') return 3
    if (number === '04') return 4
    if (number === '05') return 5
    if (number === '06') return 6
    if (number === '07') return 7
    if (number === '08') return 8
    if (number === '09') return 9
    return number
  },
  reTransformNumbers(number) {
    if (number === 0) return '00'
    if (number === 1) return '01'
    if (number === 2) return '02'
    if (number === 3) return '03'
    if (number === 4) return '04'
    if (number === 5) return '05'
    if (number === 6) return '06'
    if (number === 7) return '07'
    if (number === 8) return '08'
    if (number === 9) return '09'
    return number
  },  
}
