import Vue from 'vue'
import Router from 'vue-router'
import App from './App.vue'
import Recommendations from './components/Recommendations'
import Activities from './components/Activities'
import './main.css'
import PrintObject from 'vue-print-object'
import 'vue-print-object/dist/vue-print-object.css'

Vue.use(Router)
Vue.use(PrintObject)
Vue.config.productionTip = true

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Activities
    },
    {
      path: '/recommendations',
      name: 'Recommendations',
      component: Recommendations
    }    
  ]
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
