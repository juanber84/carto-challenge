'use strict'

import axios from 'axios'

let baseUrl = ''
if (process && process.env && process.env.NODE_ENV === 'development') {
    baseUrl = 'http://localhost:3000'
}

export default {
    async getActivities(query){
        const response = await axios.get(baseUrl + '/activities?' + query)
        const activities = response.data.data
        return activities
    },
    async getRecommendations(query) {
        const response = await axios.get(baseUrl + '/recommendations?' + query)
        const activities = response.data.data
        return activities
    },    
}