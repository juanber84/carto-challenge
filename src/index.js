'use strict'

const debug = require('debug')('app:root')

debug('generate')
const app = require('./app')
const parameters = require('./parameters')

app.listen(parameters.listenPort, '0.0.0.0', () => {
  debug('listening', parameters.listenPort)
})

module.exports = 'hello world'
