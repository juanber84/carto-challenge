'use strict'

const app = require('../../src/app')
const supertest = require('supertest')

describe('List Activities Rest Service', function() {
  beforeEach(async () => {
    this.request = supertest.agent(app)
  })

  it('GET /activities without filter must return all results', async () => {
    const response = await this.request.get('/activities')
    expect(response.body.status).toBe(true)
    expect(response.body.data.features).toHaveLength(11)
  })

  it('GET /activities with city filter must return 1 results', async () => {
    const response = await this.request.get('/activities?city=segovia')
    expect(response.body.status).toBe(true)
    expect(response.body.data.features).toHaveLength(1)
  })

  it('GET /activities with city filter must return 10 results', async () => {
    const response = await this.request.get('/activities?city=madrid')
    expect(response.body.status).toBe(true)
    expect(response.body.data.features).toHaveLength(10)
  })

  it('GET /activities with category filter must return 3 results', async () => {
    const response = await this.request.get('/activities?category=shopping')
    expect(response.body.status).toBe(true)
    expect(response.body.data.features).toHaveLength(3)
  })

  it('GET /activities with location filter must return 7 results', async () => {
    const response = await this.request.get('/activities?location=outdoors')
    expect(response.body.status).toBe(true)
    expect(response.body.data.features).toHaveLength(8)
  })

  it('GET /activities with district filter must return 2 results', async () => {
    const response = await this.request.get('/activities?district=Retiro')
    expect(response.body.status).toBe(true)
    expect(response.body.data.features).toHaveLength(2)
  })

  it('GET /activities with location and category filters must return 2 results', async () => {
    const response = await this.request.get('/activities?location=outdoors&category=shopping')
    expect(response.body.status).toBe(true)
    expect(response.body.data.features).toHaveLength(2)
  })

  it('GET /activities with location, category and district filters must return 2 results', async () => {
    const response = await this.request.get(
      '/activities?location=outdoors&category=shopping&district=Centro'
    )
    expect(response.body.status).toBe(true)
    expect(response.body.data.features).toHaveLength(2)
  })

  it('GET /activities with category filter but invalid name must return 0 results', async () => {
    const response = await this.request.get('/activities?category=fakeCategory')
    expect(response.body.status).toBe(true)
    expect(response.body.data.features).toHaveLength(0)
  })
})
