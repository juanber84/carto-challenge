'use strict'

const app = require('../../src/app')
const supertest = require('supertest')

describe('Recommendation Activities Rest Service', function() {
  beforeEach(async () => {
    this.request = supertest.agent(app)
  })

  it('GET /recommendations without filter must return 1 results', async () => {
    const response = await this.request.get('/recommendations')
    expect(response.body.status).toBe(true)
    expect(response.body.data.type).toBe('Feature')
  })

  it('GET /recommendations with category filter must return 1 results', async () => {
    const response = await this.request.get('/recommendations?category=shopping')
    expect(response.body.status).toBe(true)
    expect(response.body.data.type).toBe('Feature')
    expect(response.body.data.properties.category).toBe('shopping')
  })

  it('GET /recommendations with category filter and time-range must return 1 results', async () => {
    const response = await this.request.get(
      '/recommendations?time-range=mo-11:00-15:00&category=shopping'
    )
    expect(response.body.status).toBe(true)
    expect(response.body.data.type).toBe('Feature')
    expect(response.body.data.properties.category).toBe('shopping')
    expect(response.body.data.properties.hours_spent).toBe(2)
  })

  it('GET /recommendations with category filter, time-range and multiple must return 2 results', async () => {
    const response = await this.request.get(
      '/recommendations?time-range=mo-11:00-15:00&category=shopping&multiple=true'
    )
    expect(response.body.status).toBe(true)
    expect(response.body.data.type).toBe('Feature')
    expect(response.body.data.properties.category).toBe('shopping')
    expect(response.body.data.properties.hours_spent).toBe(2)
  })
})
