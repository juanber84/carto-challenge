'use strict'

const app = require('../../src/app')
const supertest = require('supertest')

describe('Rest', function() {
  beforeEach(async () => {
    this.request = supertest.agent(app)
  })

  it('GET /health ', async () => {
    const response = await this.request.get('/health')
    expect(response.body).toBe('hi')
  })
})
