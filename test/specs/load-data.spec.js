'use strict'

describe('Data', function() {
  it('Index', async () => {
    const madridJsonFile = require('./../../data/madrid.json')
    expect(madridJsonFile).toHaveLength(10)
  })
})
